package com.aplaza.interest.controllers;

import com.aplaza.interest.entities.Loan;
import com.aplaza.interest.entities.PaymentPlan;
import com.aplaza.interest.services.LoanCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class LoanCreateController {

    private LoanCreateService loanCreateService;

    @Autowired
    public void setLoanCreateService(LoanCreateService loanCreateService) {
        this.loanCreateService = loanCreateService;
    }

    @PostMapping(path = "/loan")
    public ResponseEntity<Object> create(@Valid @RequestBody final Loan loan) {
        List<PaymentPlan> planList = this.loanCreateService.createLoanAndPaymentPlan(loan);
        return new ResponseEntity<>(planList, HttpStatus.OK);
    }
}
