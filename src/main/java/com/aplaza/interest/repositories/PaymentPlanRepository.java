package com.aplaza.interest.repositories;

import com.aplaza.interest.entities.PaymentPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentPlanRepository extends JpaRepository<PaymentPlan, Integer> {
}
