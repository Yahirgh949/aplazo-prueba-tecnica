package com.aplaza.interest.services;

import com.aplaza.interest.entities.Loan;
import com.aplaza.interest.entities.PaymentPlan;
import com.aplaza.interest.repositories.LoanRepository;
import com.aplaza.interest.repositories.PaymentPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class LoanCreateService {

    private LoanRepository loanRepository;

    private PaymentPlanRepository paymentPlanRepository;

    @Autowired
    public void setLoanRepository(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Autowired
    public void setPaymentPlanRepository(PaymentPlanRepository paymentPlanRepository) {
        this.paymentPlanRepository = paymentPlanRepository;
    }

    public List<PaymentPlan> createLoanAndPaymentPlan(final Loan loan) {
        this.loanRepository.save(loan);
        Double amountReal = loan.getAmount() + (loan.getAmount() * (loan.getRate() / 100));
        Double payWeekly = amountReal / loan.getTerms();
        LocalDate dateControl = LocalDate.now();
        List<PaymentPlan> planList = new LinkedList<>();
        for (int i = 0; i < loan.getTerms(); i++) {
            PaymentPlan paymentPlan = new PaymentPlan();
            paymentPlan.setIdLoan(loan.getId());
            paymentPlan.setPaymentNumber(i + 1);
            paymentPlan.setPaymentAmount(payWeekly);
            paymentPlan.setPendingAmount(amountReal - payWeekly);
            amountReal = amountReal - payWeekly;
            paymentPlan.setPaymentDate(java.sql.Date.valueOf(dateControl.plusDays(7)));
            dateControl = dateControl.plusDays(7);
            planList.add(paymentPlan);
        }
        this.paymentPlanRepository.saveAll(planList);
        return planList;
    }
}
