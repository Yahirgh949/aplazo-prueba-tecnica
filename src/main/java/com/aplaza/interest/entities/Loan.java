package com.aplaza.interest.entities;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "LOANS")
@NoArgsConstructor
public class Loan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Min(value = 1, message = "El monto debe ser mayor o igual a 1.00")
    @Max(value = 999999, message = "El monto debe ser menor a 999,999.00")
    private Double amount;

    @Min(value = 4, message = "El minimo de semanas a ingresar son 4")
    @Max(value = 52, message = "El maximo de semanas a ingresar son 52")
    private Integer terms;

    @Min(value = 1, message = "El interes minimo a ingregar es del 1%")
    @Max(value = 100, message = "El interes maximo a ingresar el del 100%")
    private Double rate;

}
