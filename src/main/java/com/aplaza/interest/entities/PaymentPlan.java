package com.aplaza.interest.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "PAYMENT_PLAN")
@NoArgsConstructor
public class PaymentPlan implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_loan", insertable = false, updatable = false)
    @JsonIgnore
    private Loan loan;

    @JsonIgnore
    @Column(name = "id_loan")
    private Integer idLoan;

    private Integer paymentNumber;

    @Column(precision = 16, scale = 2)
    private Double paymentAmount;

    @Column(precision = 16, scale = 2)
    private Double pendingAmount;

    private Date paymentDate;
}
