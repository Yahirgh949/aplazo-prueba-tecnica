package com.aplaza.interest.repositories;

import com.aplaza.interest.entities.Loan;
import com.aplaza.interest.entities.PaymentPlan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class PaymentPlanRepositoryTest {

    private PaymentPlanRepository paymentPlanRepository;

    private LoanRepository loanRepository;

    @Autowired
    public void setLoanRepository(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Autowired
    public void setPaymentPlanRepository(PaymentPlanRepository paymentPlanRepository) {
        this.paymentPlanRepository = paymentPlanRepository;
    }

    @Test
    void create() {
        Loan loan= new Loan();
        loan.setAmount(1500.00);
        loan.setTerms(4);
        loan.setRate(10.00);
        this.loanRepository.save(loan);

        PaymentPlan paymentPlan = new PaymentPlan();
        paymentPlan.setPaymentDate(new Date());
        paymentPlan.setPaymentNumber(3);
        paymentPlan.setPendingAmount(1200.00);
        paymentPlan.setPaymentAmount(350.00);
        paymentPlan.setIdLoan(loan.getId());
        this.paymentPlanRepository.save(paymentPlan);
        Assertions.assertNotNull(paymentPlan.getId());

    }
}
