package com.aplaza.interest.repositories;

import com.aplaza.interest.entities.Loan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LoanRepositoryTest {

    private LoanRepository loanRepository;

    @Autowired
    public void setLoanRepository(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Test
    void createTest() {
        Loan loan= new Loan();
        loan.setAmount(1500.00);
        loan.setTerms(4);
        loan.setRate(10.00);
        this.loanRepository.save(loan);
        Assertions.assertNotNull(loan.getId());
    }


}
