package com.aplaza.interest.services;

import com.aplaza.interest.entities.Loan;
import com.aplaza.interest.entities.PaymentPlan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class LoanCreateServiceTest {

    @Autowired
    private LoanCreateService loanCreateService;



    @Test
    void createPlan() {
        Loan loan = new Loan();
        loan.setAmount(1500.00);
        loan.setRate(50.0);
        loan.setTerms(4);
        List<PaymentPlan> planList = this.loanCreateService.createLoanAndPaymentPlan(loan);
        Assertions.assertNotNull(planList);
    }
}
